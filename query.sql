-- Make table user_game 
CREATE TABLE user_game(
    user_game_id SERIAL PRIMARY KEY,
    user_game_username VARCHAR (255) NOT NULL,
    user_game_password VARCHAR(255) NOT NULL
);

-- Make table for user_game bio
CREATE TABLE 


'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_game.init({
    user_game_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_game_username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_game_password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    sequelize,
    tableName: 'user_game',
    modelName: 'user_game',
  });
  return user_game;
};



'use strict';
/** @type {import('sequelize-cli').Migration} */
const { DataTypes } = require("sequelize");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('user_game', {
      // id table
      user_game_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      user_game_username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      user_game_password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_game');
  }
};