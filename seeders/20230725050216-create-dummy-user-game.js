'use strict';

/** @type {import('sequelize-cli').Migration} */
const { v4: uuidv4 } = require("uuid");
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const userGameRow = await queryInterface.bulkInsert(
      "user_game",
      [
        {
          userGameUsername: 'John Doe',
          userGamePassword: 'admin',
          uuid: uuidv4(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
        {returning: true}
      );
      // the 1st data that was inserted, which is the onlyy data
      const userGameId = userGameRow[0].userGameId;
       // Insert data into "user_game_bios" table
    await queryInterface.bulkInsert("user_game_bios", 
    [
      {
        userGameBioId: 1,
        uuid: uuidv4(),
        userGameId: userGameId,
        name: "John",
        email: "John@doe.com",
        gender: "male",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
 // Insert data into "user_game_history" table
    await queryInterface.bulkInsert("user_game_history", 
    [
      { 
        // dont hard code the id.its auto
        uuid: uuidv4(),
        userGameId: userGameId,
        win: 10,
        loss: 20,
        timePlayhrs: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
    },
  ]);
      
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
