'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_history.belongsTo(models.user_game, {
        foreignKey: 'userGameId',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      });
    }
  }
  user_game_history.init({
    userGameHistoryId:{
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
     userGameId: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    uuid: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
    },

    win: DataTypes.INTEGER,
    loss: DataTypes.INTEGER,
    timePlayhrs: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'user_game_history',
    modelName: 'user_game_history',
  });
  return user_game_history;
};